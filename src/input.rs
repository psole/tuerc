pub mod helper {
    use std::path::Path;

    use clap::Parser;
    use rand::{thread_rng, Rng};
    use url::Url;

    #[derive(Parser)]
    #[command(author, version, about, long_about = None, about = "
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣟⠀⠀⠀⠀⠈⠿⢇⣽⡇⠀⠀⠀⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⡴⢾⠃⠀⠀⠀⠀⠀⠀⠀⠘⠃⠀⠀⠀⠹⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⡤⠖⣻⠉⠁⠈⠳⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢹⠙⢯⡓⠒⠲⠶⠶⠦⢤⣤⣀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣀⡠⠖⠊⠁⠀⢠⡇⠀⠀⠀⠀⠈⠑⠦⣀⣀⣀⣀⣀⡀⠀⢀⣠⠖⠁⠀⠀⠉⢳⡀⠀⠀⠀⠀⠀⠈⠉⠙⠲⠦⣄⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣀⡤⠔⠋⠁⠀⠀⠀⠀⠀⢿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠉⠉⠀⠀⠀⠀⠀⠀⠀⣳⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠹⣆⠀
⠀⠀⠀⢀⡤⠖⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⢧⠀⠀⠀⠀⠀ t u e r c ⠀⠀⠀⠀⠀⢠⡏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢹⡀
⠀⢀⡞⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⢷⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⠏⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⣇
⢀⡿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⢦⡀⠀⠀⠀⠀⠀⠀⢀⣶⠀⠀⠀⠀⠀⠀⢀⡼⠋⠀⠀⠀⠀⢀⣀⠴⣶⠲⠶⠀⠀⠀⠀⠈⣿
⢸⡇⠀⠀⠀⠀⠙⠻⡶⠶⢤⣀⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⣓⣦⣄⣀⣠⠴⠟⠉⠳⠤⣀⣀⣠⣴⣋⣀⣠⠤⠶⠚⠋⠉⠀⠀⣿⠀⢀⣀⣀⣀⣠⣐⣿
⠘⢿⡀⠀⠀⠀⠀⠀⣷⠀⠀⠀⠀⠉⠙⠛⠛⠒⠛⠛⠋⠉⠉⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣯⣭⣿⣷⣿⣷⣤⡟
⠀⠈⣷⠶⢦⣤⣶⣒⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢹⣟⣛⣿⣿⣿⠿⣾⡇
    ")]
    struct Cli {
        /// The desired youtube URL, with escaped special characters (i e '?' or '=')
        url: Url,

        /// The filename of the expected MP3 file
        #[arg(short = 'f', long)]
        filename: Option<String>,

        /// The path to the yt-dlp executable file
        #[arg(long)]
        yt_dlp: Option<String>,

        /// The path to the youtube-dl executable file
        #[arg(long)]
        youtube_dl: Option<String>,

        /// The path to the ffmpeg executable file
        #[arg(long)]
        ffmpeg: Option<String>,
    }

    /// Holds the data the user can input.
    pub struct ParsedArgs {
        pub url: String,
        pub yt_dl_location: String,
        pub ffmpeg_location: String,
        pub filename: String,
    }

    /// Entrypoint. Checks all arguments.
    pub fn parse_args() -> Result<ParsedArgs, String> {
        let args = Cli::parse();

        let filename = get_filename(args.filename);
        check_url(&args.url)?;
        let url = args.url.to_string();
        let yt_dl_location = get_yt_dl_bin_path(args.yt_dlp, args.youtube_dl)?;
        let ffmpeg_location = get_ffmpeg_bin_path(args.ffmpeg)?;

        return Ok(ParsedArgs {
            url,
            yt_dl_location,
            ffmpeg_location,
            filename,
        });
    }

    /// Returns either an unwrapped filename from args if present, or a random generated one.
    fn get_filename(filename: Option<String>) -> String {
        if let Some(filename) = filename {
            return filename;
        }

        let mut rng = thread_rng();
        let random_number = rng.gen_range(10000000..99999999);
        return random_number.to_string();
    }

    /// Checks the provided URL's domain is youtube with a https scheme.
    fn check_url(url: &Url) -> Result<(), String> {
        const YOUTUBE_DOMAIN: &str = "www.youtube.com";
        const HTTPS_SCHEME: &str = "https";

        let domain = match url.domain() {
            Some(domain) => domain,
            None => return Err("Unable to extract domain".to_string()),
        };

        let scheme = url.scheme();

        if domain != YOUTUBE_DOMAIN {
            return Err("Invalid domain provided".to_string());
        }
        if scheme != HTTPS_SCHEME {
            return Err("Invalid scheme provided".to_string());
        }
        return Ok(());
    }

    /// Returns a valid youtube-dl path.
    /// Returns error if an invalid custom path is provided,
    /// or if neither youtube-dl nor yt-dlp aren't installed.
    fn get_yt_dl_bin_path(
        yt_dlp_arg: Option<String>,
        youtube_dl_arg: Option<String>,
    ) -> Result<String, String> {
        const YT_DLP_BIN: &str = "yt-dlp";
        const YOUTUBE_DL_BIN: &str = "youtube-dl";
        const YT_DLP_DIRECTORY: &str = "/usr/bin";
        const YOUTUBE_DL_DIRECTORY: &str = "/usr/bin";

        if let Some(yt_dlp_location) = yt_dlp_arg {
            return match check_bin_file(&YT_DLP_BIN, &yt_dlp_location.as_str()) {
                Ok(_) => Ok(yt_dlp_location),
                Err(e) => Err(e),
            };
        }

        if let Some(youtube_dl_location) = youtube_dl_arg {
            return match check_bin_file(&YOUTUBE_DL_BIN, &youtube_dl_location.as_str()) {
                Ok(_) => Ok(youtube_dl_location),
                Err(e) => Err(e),
            };
        }

        let yt_dlp_location = format!("{}/{}", YT_DLP_DIRECTORY, YT_DLP_BIN);
        if check_bin_file(&YT_DLP_BIN, &yt_dlp_location).is_ok() {
            return Ok(yt_dlp_location.to_string());
        }

        let youtube_dl_location = format!("{}/{}", YOUTUBE_DL_DIRECTORY, YOUTUBE_DL_BIN);
        if check_bin_file(&YOUTUBE_DL_BIN.to_string(), &youtube_dl_location).is_ok() {
            return Ok(youtube_dl_location.to_string());
        }

        Err("yt-dlp or youtube-dl must be installed".to_string())
    }

    /// Returns a valid ffmpeg path.
    /// Returns error if an invalid custom path is provided or if ffmpeg isn't installed.
    fn get_ffmpeg_bin_path(ffmpeg_arg: Option<String>) -> Result<String, String> {
        const FFMPEG_BIN: &str = "ffmpeg";
        const FFMPEG_DIRECTORY: &str = "/usr/bin";

        if let Some(ffmpeg_location) = ffmpeg_arg {
            return match check_bin_file(&FFMPEG_BIN, &ffmpeg_location.as_str()) {
                Ok(_) => return Ok(ffmpeg_location),
                Err(e) => Err(e),
            };
        }

        let ffmpeg_location = format!("{}/{}", FFMPEG_DIRECTORY, FFMPEG_BIN);
        if check_bin_file(&FFMPEG_BIN, &ffmpeg_location).is_ok() {
            return Ok(ffmpeg_location.to_string());
        }

        return Err("ffmpeg must be installed".to_string());
    }

    /// Checks the provided path to a bin file exists, and if the bin file matches the expected one.
    fn check_bin_file(expected_bin: &str, bin_file_location: &str) -> Result<(), String> {
        let bin_file_path = Path::new(bin_file_location);

        if !bin_file_path.exists() {
            return Err(format!(
                "The path {} does not exist",
                bin_file_path.display()
            ));
        }

        let optional_component = bin_file_path.components().last();
        if optional_component.is_none() {
            return Err("Unable to recover executable from path".to_string());
        }
        let optional_str = optional_component.unwrap().as_os_str().to_str();
        if optional_str.is_none() {
            return Err("Unable to recover executable from path".to_string());
        }

        if optional_str.unwrap() != expected_bin {
            return Err(format!(
                "The provided path must be a {} binary",
                expected_bin,
            ));
        }

        Ok(())
    }
}
