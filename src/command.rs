pub mod helper {
    use std::process::{Command, Stdio};

    const EXPECTED_AUDIO_FORMAT: &str = "opus";

    /// Handles the execution of the youtube command.
    /// It will end up downloading the specified URL and extracting its audio on the best
    /// available audio format.
    pub fn run_yt_dl_command(
        yt_dl_bin: &String,
        url: &String,
        filename: &String,
    ) -> Result<(), String> {
        const EXTRACT_AUDIO_OPTION: &str = "-x";
        const FORMAT_OPTION: &str = "-f";
        const DEFAULT_FORMAT_VALUE: &str = "bestaudio";
        const OUTPUT_OPTION: &str = "-o";

        let result = Command::new(yt_dl_bin)
            .arg(url)
            .arg(EXTRACT_AUDIO_OPTION)
            .args([FORMAT_OPTION, DEFAULT_FORMAT_VALUE])
            .args([OUTPUT_OPTION, filename])
            .stdout(Stdio::null())
            .status();

        let status = match result {
            Ok(status) => status,
            Err(error) => {
                return Err(format!(
                    "Failed to execute download command. Error: {}",
                    error
                ))
            }
        };

        if !status.success() {
            return Err("The download command run, but returned an error code".to_string());
        }

        Ok(())
    }

    pub fn run_ffmpeg_command(ffmpeg_bin: &String, filename: &String) -> Result<(), String> {
        const DEFAULT_FFMPEG_EXTENSION: &str = "mp3";
        const INPUT_OPTION: &str = "-i";

        let in_file = format!("{}.{}", filename, EXPECTED_AUDIO_FORMAT);
        let out_file = format!("{}.{}", filename, DEFAULT_FFMPEG_EXTENSION);

        let result = Command::new(ffmpeg_bin)
            .args([INPUT_OPTION, in_file.as_str()])
            .arg(out_file)
            .stdout(Stdio::null())
            .stderr(Stdio::null())
            .stdin(Stdio::null())
            .status();

        let status = match result {
            Ok(status) => status,
            Err(error) => {
                return Err(format!(
                    "Failed to execute encoding command. Error: {}",
                    error
                ))
            }
        };

        if !status.success() {
            return Err("The download command run, but returned an error code".to_string());
        }

        Ok(())
    }

    pub fn run_rm_command(filename: &String) -> Result<(), String> {
        const RM_BIN: &str = "rm";

        let in_file = format!("{}.{}", filename, EXPECTED_AUDIO_FORMAT);

        let result = Command::new(RM_BIN).arg(in_file).status();
        let status = match result {
            Ok(status) => status,
            Err(error) => {
                return Err(format!("Failed to execute rm command. Error: {}", error));
            }
        };

        if !status.success() {
            return Err("The rm command run, but returned an error code".to_string());
        }

        Ok(())
    }
}
