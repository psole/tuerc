pub mod command;
pub mod input;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = input::helper::parse_args()?;

    println!("Starting download...");
    
    command::helper::run_yt_dl_command(&args.yt_dl_location, &args.url, &args.filename)?;
    
    println!("Download completed. Starting encoding...");
    
    command::helper::run_ffmpeg_command(&args.ffmpeg_location, &args.filename)?;

    println!("Encoding completed. Starting cleaning...");

    command::helper::run_rm_command(&args.filename)?;

    println!("Finished downloading {} successfully.", args.filename);
    Ok(())
}
