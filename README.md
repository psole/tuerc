# tuerc
A project made to introduce myself to Rust while helping my girlfriend download mp3 files from YouTube links.<br>

### System requirements
- ffmpeg (tested with version n6.0)
- youtube-dl or yt-dlp (tested with versions 2021.12.17 and 2023.03.04)

### Installation
The program can be compiled as any other Rust simple program.<br>
I compiled a binary using cargo. `$ cargo build --release`<br>


### Usage
`$ tuerc [OPTIONS] [URL]`<br>
`$ tuerc --help` for more information.<br>